#!/usr/bin/env bash
# This script can be used as an example of a script can be copied to a remote host using SFTP in a pre-hook
uname -a;
uptime;
date;
exit 0;
